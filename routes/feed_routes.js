/**
 * Created by student on 4/12/17.
 */
var express = require('express');
var router = express.Router();
var feed_dal = require('../model/feed_dal');


// View All feeds
router.get('/all', function(req, res) {
    feed_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('feed/feedViewAll', { 'result':result });
        }
    });

});

// View the feed for the given id
router.get('/', function(req, res){
    if(req.query.feed_name == null) {
        res.send('feed_name is null');
    }
    else {
        feed_dal.getById(req.query.feed_name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('feed/feedViewById', {'result': result});
            }
        });
    }
});


// Return the add a new feed form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    feed_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('feed/feedAdd', {'email': result});
        }
    });
});

// View the feed for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.feed_regiment == "") {
        res.send('Feed regiment must be provided.');
    }
    if(req.query.feed_name == "") {
        res.send('Feed Name must be provided.');
    }
    if(req.query.bag_size_lb == "") {
        res.send('Bag Size must be provided.');
    }
    else if(req.query.cost == "") {
        res.send('Cost must be provided');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        feed_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/feed/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.feed_name == null) {
        res.send('A feed id is required');
    }
    else {
        feed_dal.edit(req.query.feed_name, function(err, result){
            res.render('feed/feedUpdate', {feed: result[0][0]});
        });
    }

});


router.get('/edit2', function(req, res){
    if(req.query.feed_name == null) {
        res.send('A feed id is required');
    }
    else {
        feed_dal.getById(req.query.feed_name, function(err, feed){
            res.render('feed/feedUpdate', {feed: feed[0]});
        });
    }

});


router.get('/update', function(req, res) {
    feed_dal.update(req.query, function(err, result){
        res.redirect(302, '/feed/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.feed_name == null) {
        res.send('feed_name is null');
    }
    else {
        feed_dal.delete(req.query.feed_name, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/feed/all');
            }
        });
    }
});

module.exports = router;