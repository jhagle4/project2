/**
 * Created by student on 4/12/17.
 */
var express = require('express');
var router = express.Router();
var field_dal = require('../model/field_dal');


// View All fields
router.get('/all', function(req, res) {
    field_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('field/fieldViewAll', { 'result':result });
        }
    });

});

// View the field for the given id
router.get('/', function(req, res){
    if(req.query.location == null) {
        res.send('field_name is null');
    }
    else {
        field_dal.getById(req.query.location, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('field/fieldViewById', {'result': result});
            }
        });
    }
});


// Return the add a new field form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    field_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('field/FieldAdd', {'location': result});
        }
    });
});

router.get('/addAnimal', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    field_dal.editAnimal(req.query.location,  function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('field/FieldAddAnimal', {'field': req.query.location, 'animal_name': result});
        }
    });
});

// View the field for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.location == "") {
        res.send('location must be provided.');
    }
    if(req.query.field_type == "") {
        res.send('Field typ must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        field_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/field/all');
            }
        });
    }
});

router.get('/insertAnimal', function(req, res){
    // simple validation
    if(req.query.location == "") {
        res.send('location must be provided.');
    }
    if(req.query.field_type == "") {
        res.send('Field type must be provided.');
    }
    if(req.query.animal_name == "") {
        res.send('Animle Name must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        field_dal.insertAnimal(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/field/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.location == null) {
        res.send('A field id is required');
    }
    else {
        field_dal.edit(req.query.location, function(err, field){
            res.render('field/FieldUpdate', {field: field[0][0]});
        });
    }

});

router.get('/editAnimal', function(req, res){
    if(req.query.location == null) {
        res.send('A field id is required');
    }
    else {
        field_dal.editAnimal(req.query.location, function(err, field){
            res.render('field/FieldUpdate', {field: field[0][0]});
        });
    }

});


router.get('/edit2', function(req, res){
    if(req.query.field_name == null) {
        res.send('A field id is required');
    }
    else {
        field_dal.getById(req.query.field_name, function(err, field){
            res.render('field/fieldUpdate', {field: field[0]});
        });
    }

});


router.get('/update', function(req, res) {
    field_dal.update(req.query, function(err, result){
        res.redirect(302, '/field/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.location == null) {
        res.send('location is null');
    }
    else {
        field_dal.delete(req.query.location, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/field/all');
            }
        });
    }
});

router.get('/deleteAnimal', function(req, res){
    if(req.query.animal_name == null) {
        res.send('animal_name is null');
    }
    else {
        field_dal.deleteAnimal(req.query.animal_name, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/field/all');
            }
        });
    }
});

module.exports = router;