/**
 * Created by student on 4/12/17.
 */
var express = require('express');
var router = express.Router();
var atype_dal = require('../model/atype_dal');


// View All Animal_Types
router.get('/all', function(req, res) {
    atype_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('Animal_type/animal_typeViewAll', { 'result':result });
        }
    });

});

// View the Animal_Type for the given id
router.get('/', function(req, res){
    if(req.query.animal_type_name == null) {
        res.send('animal_type_name is null');
    }
    else {
        atype_dal.getById(req.query.animal_type_name, function(err,result, animal_type) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('Animal_Type/Animal_TypeViewById', {'result': result, 'type': animal_type});
            }
        });
    }
});

// View the Animal for the given id
router.get('/animal', function(req, res){
    if(req.query.animal_name == null) {
        res.send('animal_name is null');
    }
    else {
        atype_dal.getAnimalById (req.query.animal_name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('Animal_Type/AnimalViewById', {'result': result});
            }
        });
    }
});

// Return the add a new Animal_Type form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    atype_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Animal_Type/animalTypeAdd', {'animal_type_name': result});
        }
    });
});

// Return the add a new Animal_Type form
router.get('/AnimalAdd', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    atype_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('Animal_Type/animalAdd', {'animal_name': result, 'type': req.query.animal_type_name});
        }
    });
});

// View the Animal_Type for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.animal_type_name == "") {
        res.send('Animal Type must be provided.');
    }

    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        atype_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/atype/all');
            }
        });
    }
});

router.get('/insertAnimal', function(req, res){
    // simple validation
    if(req.query.animal_type_name == "") {
        res.send('Animal Type name must be provided.');
    }
    if(req.query.animal_name == "") {
        res.send('Animal name must be provided.');
    }
    if(req.query.breed == "") {
        res.send('Breed must be provided.');
    }
    if(req.query.gender == "") {
        res.send('Gender must be provided.');
    }
    if(req.query.Nchildren == "") {
        res.send('Animal Type must be provided.');
    }
    if(req.query.cost == "") {
        res.send('Cost must be provided.');
    }
    if(req.query.feed_regiment == "") {
        res.send('feed_regiment must be provided.');
    }

    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        atype_dal.insertAniaml (req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/atype/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.animal_name == null) {
        res.send('A Animal_name is required');
    }
    else {
        atype_dal.edit(req.query.animal_name, function(err, result){
            res.render('Animal_Type/animalUpdate', {Animal: result[0][0]});
        });
    }

});


router.get('/edit2', function(req, res){
    if(req.query.animal_type_name == null) {
        res.send('A Animal_Type id is required');
    }
    else {
        atype_dal.getById(req.query.animal_type_name, function(err, Animal_Type){
            res.render('Animal_Type/Animal_TypeUpdate', {Animal_Type: Animal_Type[0]});
        });
    }

});


router.get('/update', function(req, res) {
    atype_dal.update(req.query, function(err, result){
        res.redirect(302, '/atype/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.animal_type_name == null) {
        res.send('animal_type_name is null');
    }
    else {
        atype_dal.getById(req.query.animal_type_name, function(err,result1, animal_type) {
            atype_dal.delete(req.query.animal_type_name, result1, function (err, result2) {
                if (err) {
                    res.send(err);
                }
                else {
                    //poor practice, but we will handle it differently once we start using Ajax
                    res.redirect(302, '/atype/all');
                }
            });
        })
    }
});

router.get('/deleteAnimal', function(req, res){
    if(req.query.animal_name == null) {
        res.send('animal_name is null');
    }
    else {
        atype_dal.deleteAnimal(req.query.animal_name, function (err, result) {
                if (err) {
                    res.send(err);
                }
                else {
                    //poor practice, but we will handle it differently once we start using Ajax
                    res.redirect(302, '/atype/all');
                }
        });
    }
});

module.exports = router;