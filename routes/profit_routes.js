/**
 * Created by student on 4/12/17.
 */
var express = require('express');
var router = express.Router();
var profit_dal = require('../model/profit_dal');


// View All profits
router.get('/all', function(req, res) {
    profit_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('profit/profitViewAll', { 'result':result });
        }
    });

});

// View the profit for the given id
router.get('/', function(req, res){
    if(req.query.animal_name == null) {
        res.send('animal_name is null');
    }
    else {
        profit_dal.getById(req.query.animal_name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('profit/profitViewById', {'result': result});
            }
        });
    }
});

router.get('/having', function(req, res) {
    profit_dal.getHaving(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('profit/profitHaving', { 'result':result });
        }
    });

});


// Return the add a new profit form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    profit_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('profit/profitAdd', {'email': result});
        }
    });
});

// View the profit for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.first_name == "") {
        res.send('Persons First Name must be provided.');
    }
    if(req.query.last_name == "") {
        res.send('Persons Last Name must be provided.');
    }
    else if(req.query.email == "") {
        res.send('At least one email must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        profit_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/profit/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.profit_name == null) {
        res.send('A profit id is required');
    }
    else {
        profit_dal.edit(req.query.profit_name, function(err, result){
            res.render('profit/profitUpdate', {profit: result[0][0]});
        });
    }

});


router.get('/edit2', function(req, res){
    if(req.query.profit_name == null) {
        res.send('A profit id is required');
    }
    else {
        profit_dal.getById(req.query.profit_name, function(err, profit){
            res.render('profit/profitUpdate', {profit: profit[0]});
        });
    }

});


router.get('/update', function(req, res) {
    profit_dal.update(req.query, function(err, result){
        res.redirect(302, '/profit/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.profit_name == null) {
        res.send('profit_name is null');
    }
    else {
        profit_dal.delete(req.query.profit_name, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/profit/all');
            }
        });
    }
});

router.get('/profitViewAll', function(req, res){
    profit_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('profit/profitViewAll', {'profit': result});
        }
    });
});

module.exports = router;