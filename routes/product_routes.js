/**
 * Created by student on 4/12/17.
 */
var express = require('express');
var router = express.Router();
var product_dal = require('../model/product_dal');


// View All products
router.get('/all', function(req, res) {
    product_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('product/productViewAll', { 'result':result });
        }
    });

});

// View the product for the given id
router.get('/', function(req, res){
    if(req.query.animal_name == null) {
        res.send('animal_name is null');
    }
    else {
        product_dal.getById(req.query.animal_name, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('product/productViewById', {'result': result});
            }
        });
    }
});


// Return the add a new product form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    product_dal.addPage(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('product/ProductAdd', {'product_name': result});
        }
    });
});

// View the product for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.product_name == "") {
        res.send('product Name must be provided.');
    }
    if(req.query.animal_name == "") {
        res.send('animal Name must be provided.');
    }
    else if(req.query.price == "") {
        res.send('price must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        product_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/atype/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.product_name == null) {
        res.send('A product id is required');
    }
    else {
        product_dal.edit(req.query.product_name, function(err, result){
            res.render('product/productUpdate', {product: result[0][0]});
        });
    }

});


router.get('/edit2', function(req, res){
    if(req.query.product_name == null) {
        res.send('A product id is required');
    }
    else {
        product_dal.getById(req.query.product_name, function(err, product){
            res.render('product/productUpdate', {product: product[0]});
        });
    }

});


router.get('/update', function(req, res) {
    product_dal.update(req.query, function(err, result){
        res.redirect(302, '/product/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.product_name == null) {
        res.send('product_name is null');
    }
    else {
        product_dal.delete(req.query.product_name, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/atype/all');
            }
        });
    }
});

router.get('/productViewAll', function(req, res){
    product_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('product/productViewAll', {'product': result});
        }
    });
});

module.exports = router;