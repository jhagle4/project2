/**
 * Created by student on 4/12/17.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT Distinct location FROM Field;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(location, callback) {
    var query = 'SELECT * FROM Field where location = ?';
    var queryData = [location];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO Field (location, field_type) VALUES (?)';

    var queryData = [params.location, params.field_type];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.insertAnimal = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO Field (location, field_type, animal_name) VALUES (?)';
    var query2 = 'Delete from Field where animal_name = ?';

    var queryData = [params.location, params.field_type, params.animal_name];
    var queryData2 = [""];

    connection.query(query, [queryData], function(err, result) {
        connection.query(query2, [queryData2], function (err, result2) {
            callback(err, result);
        })
    });

};

exports.delete = function(location, callback) {
    var query = 'DELETE FROM Field where location = ?';
    var queryData = [location];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.deleteAnimal = function(animal_name, callback) {
    var query = 'DELETE FROM Field where animal_name = ?';
    var queryData = [animal_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

/*
//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    for(var i=0; i < addressIdArray.length; i++) {
        companyAddressData.push([company_id, addressIdArray[i]]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

*/
exports.update = function(params, callback) {
    var query = 'UPDATE Field SET field_type = ? WHERE location = ?';
    var queryData = [params.field_type, params.location];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS field_getinfo;

 DELIMITER //
 CREATE PROCEDURE field_getinfo (_field_name int)
 BEGIN

 SELECT * FROM field WHERE field_name = _field_name;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL field_getinfo (4);

 */

exports.edit = function(location, callback) {
    var query = 'CALL field_getinfo(?)';
    var queryData = [location];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.editAnimal = function(location, callback) {
    var query = 'CALL field_animal_getinfo(?)';
    var queryData = [location];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};