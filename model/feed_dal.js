/**
 * Created by student on 4/12/17.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT Distinct feed_name FROM Feed;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(feed_name, callback) {
    var query = 'SELECT * FROM Feed where feed_name = ?';
    var queryData = [feed_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO Feed (feed_regiment, feed_name, bag_size_lb, cost) VALUES (?)';

    var queryData = [params.feed_regiment, params.feed_name, params.bag_size_lb, params.cost];

    connection.query(query, [queryData], function (err, result) {
        callback(err, result);
    });

};

exports.delete = function(feed_name, callback) {
    var query = 'DELETE FROM Feed where feed_name = ?';
    var queryData = [feed_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

/*
//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    for(var i=0; i < addressIdArray.length; i++) {
        companyAddressData.push([company_id, addressIdArray[i]]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

*/
exports.update = function(params, callback) {
    var query = 'UPDATE Feed SET feed_name = ?, bag_size_lb = ?, cost = ? WHERE feed_regiment = ?';
    var queryData = [params.feed_name, params.bag_size_lb, params.cost, params.feed_regiment];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS feed_getinfo;

 DELIMITER //
 CREATE PROCEDURE feed_getinfo (_feed_name int)
 BEGIN

 SELECT * FROM feed WHERE feed_name = _feed_name;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL feed_getinfo (4);

 */

exports.edit = function(feed_name, callback) {
    var query = 'CALL feed_getinfo(?)';
    var queryData = [feed_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};