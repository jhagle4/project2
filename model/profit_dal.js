/**
 * Created by student on 4/12/17.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM Profit;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getHaving = function(callback) {
    var query = 'SELECT *, sum(gain - cost) as total FROM Profit ' +
        'group by animal_name having total > 0;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(animal_name, callback) {
    var query = 'SELECT *, sum(gain - cost) as total FROM Profit where animal_name = ?';
    var queryData = [animal_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO profit (profit_name, animal_name) VALUES (?)';

    var queryData = [params.profit_name, params.animal_name];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(profit_name, callback) {
    var query = 'DELETE FROM profit where profit_name = ?';
    var queryData = [profit_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

/*
//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    for(var i=0; i < addressIdArray.length; i++) {
        companyAddressData.push([company_id, addressIdArray[i]]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

*/
exports.update = function(params, callback) {
    var query = 'UPDATE profit SET animal_name = ? WHERE profit_name = ?';
    var queryData = [params.animal_name, params.profit_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS profit_getinfo;

 DELIMITER //
 CREATE PROCEDURE profit_getinfo (_profit_name int)
 BEGIN

 SELECT * FROM profit WHERE profit_name = _profit_name;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL profit_getinfo (4);

 */

exports.edit = function(profit_name, callback) {
    var query = 'CALL resume_id_getinfo(?)';
    var queryData = [profit_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};