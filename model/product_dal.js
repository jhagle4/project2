/**
 * Created by student on 4/12/17.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT Distinct animal_name FROM Product;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(animal_name, callback) {
    var query = 'SELECT * FROM Product where animal_name = ?';
    var queryData = [animal_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO Product (product_name, animal_name, price) VALUES (?)';

    var queryData = [params.product_name, params.animal_name, params.price];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(product_name, callback) {
    var query = 'DELETE FROM Product where product_name = ?';
    var queryData = [product_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

/*
//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    for(var i=0; i < addressIdArray.length; i++) {
        companyAddressData.push([company_id, addressIdArray[i]]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

*/
exports.update = function(params, callback) {
    var query = 'UPDATE product SET animal_name = ? WHERE product_name = ?';
    var queryData = [params.animal_name, params.product_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS product_getinfo;

 DELIMITER //
 CREATE PROCEDURE product_getinfo (_product_name int)
 BEGIN

 SELECT * FROM product WHERE product_name = _product_name;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL product_getinfo (4);

 */

exports.edit = function(product_name, callback) {
    var query = 'CALL resume_id_getinfo(?)';
    var queryData = [product_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.addPage = function (callback) {
    var query = 'SELECT animal_name FROM Animal_Type';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};