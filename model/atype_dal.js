/**
 * Created by student on 4/12/17.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT Distinct animal_type_name FROM Animal_Type;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(animal_type_name, callback) {
    var query = 'SELECT a.animal_name FROM Animal a ' +
        'left join Animal_Type atype on a.animal_name = atype.animal_name' +
        ' where animal_type_name = ?';
    var queryData = [animal_type_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result, animal_type_name);
    });
};

exports.getAnimalById = function(animal_name, callback) {
    var query = 'SELECT * FROM Animal where animal_name = ?';
    var queryData = [animal_name];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};



exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO Animal_Type (animal_type_name) VALUES (?)';

    var queryData = [params.animal_type_name];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.insertAniaml = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO Animal (animal_name, breed, gender, Nchildren, cost, feed_regiment) VALUES (?)';
    var query2 = 'INSERT INTO Animal_Type (animal_type_name, animal_name) VALUES(?)';
    var query3 = 'DELETE FROM Animal_Type where animal_name = null';

    var queryData = [params.animal_name, params.breed, params.gender, params.Nchildren, params.cost, params.feed_regiment];
    var queryData2 = [params.animal_type_name, params.animal_name];
    var queryData3 = [params.animal_type_name];

    connection.query(query, [queryData], function(err, result) {
        connection.query(query2, [queryData2], function(err, result) {
            connection.query(query3, [queryData3], function(err, result) {
                callback(err, result);
            })
        })
    });

};

exports.delete = function(animal_type_name, names, callback) {
    var query = 'DELETE FROM Animal_Type where animal_type_name = ?';
    var query2 = 'Delete FROM Animal where animal_name = ?';

    var queryData = [animal_type_name];

    connection.query(query, queryData, function(err, result) {
        for(var i=0; names.length > i; i++ ) {
            var queryData2 = [names[i]];
            connection.query(query2, [queryData2], function (err, result) {
            })
        }
        callback(err, result);
    });

};

exports.deleteAnimal = function(animal_name, callback) {
    var query = 'DELETE FROM Animal_Type where animal_name = ?';
    var query2 = 'Delete FROM Animal where animal_name = ?';

    var queryData = [animal_name];

    connection.query(query, queryData, function(err, result) {
        connection.query(query2, queryData, function(err, result) {
            callback(err, result);
        })
    });

};

/*
//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    for(var i=0; i < addressIdArray.length; i++) {
        companyAddressData.push([company_id, addressIdArray[i]]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};

//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

*/
exports.update = function(params, callback) {
    var query = 'UPDATE Animal SET breed = ?, gender = ?, Nchildren =?, cost = ?, feed_regiment = ? WHERE animal_name = ?';
    var queryData = [params.breed, params.gender, params.Nchildren, params.cost, params.feed_regiment, params.animal_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS animal_name_getinfo;

 DELIMITER //
 CREATE PROCEDURE animal_name_getinfo (_animal_name Text)
 BEGIN

 SELECT * FROM Animal WHERE animal_name = _animal_name;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL animal_name_getinfo (S2);

 */

exports.edit = function(animal_name, callback) {
    var query = 'CALL animal_name_getinfo(?)';
    var queryData = [animal_name];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};